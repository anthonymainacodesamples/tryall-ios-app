# Tryall iOS App

Code repository by Anthony Maina

### Languages ###
Swift 3

### Quick summary 
iOS Companion app to flagship product that allows users to browse free trials and receive notifications on their mobile devices.
Communicates with existing node server from main web app. 

### Version ###
Beta

### How do I get set up? ###

To see it in action you would have to run the main server then launch the app as the app is not live and available for the public.
