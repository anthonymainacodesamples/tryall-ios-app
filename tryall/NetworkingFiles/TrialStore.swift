//
//  TrialStore.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 11/14/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import GoogleSignIn

enum TrialsResults {
    case success([Trial])
    case failure(Error)
}

enum CategoriesResults {
    case success([Interest])
    case failure(Error)
}

enum SearchResults {
    case success([SearchResult])
    case failure(Error)
}

enum SearchError: Error {
    case invalidJSONData
}
enum TrialError:Error {
    case invalidJSONData
}

enum CategoryError:Error {
    case invalidJSONData
}


class TrialStore {
    
    private func processTrialsRequest(data: Data?, error:Error?) ->  TrialsResults {
        
        guard let jsonData = data else {
             return .failure(error!)
        }
        return TryallAPI.trials(fromJSON: jsonData)
    }
    
    private func processFrequencyRequest(data: Data?, error:Error?) ->  Bool {
        guard let jsonData = data else {
            return false
        }
        return TryallAPI.frequencyVal(fromJSON: jsonData)!
    }
    private func processRemindersRequest(specificVal: String,data: Data?, error:Error?) ->  Bool {
        guard let jsonData = data else {
            return false
        }
        return TryallAPI.reminderVal(specificVal: specificVal, fromJSON: jsonData)!
        
    }
    
    
    private func processSearchRequest(data: Data?, error:Error?) ->  SearchResults {
        
        guard let jsonData = data else {
            return .failure(error!)
        }
        return TryallAPI.searches(fromJSON: jsonData)
    }
    
    private func processCategoriesRequest(data: Data?, error:Error?) ->  CategoriesResults {
        
        guard let jsonData = data else {
            return .failure(error!)
        }
        return TryallAPI.categories(fromJSON: jsonData)
    }
    
    func fetchSearchResults(searchText: String, completion: @escaping (SearchResults) -> Void){
        let url = TryallAPI.retrieveSearchResults.absoluteString

        let parameters = ["searchTerm":searchText]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            if let jsonData = response.data {
                let result = self.processSearchRequest(data: jsonData, error: response.result as? Error)
                OperationQueue.main.addOperation {
                    completion(result)
                }
            }
        }
    }
    
    func fetchSimilarTrials(id: String, completion: @escaping (TrialsResults) -> Void){
        let url = TryallAPI.retrieveSimilarTrials.absoluteString
        let final = url + id
        print(final)
        Alamofire.request(final).responseJSON { response in
            if let jsonData = response.data {
                let result = self.processTrialsRequest(data: jsonData, error: response.result as? Error)
                OperationQueue.main.addOperation {
                    completion(result)
                }
            }
        }
    }
    
    func fetchSingleTrial(id:String,completion: @escaping (TrialsResults) -> Void) {
        //Get url value
        let urlString = TryallAPI.retrieveSingleTrial.absoluteString
        //Append provided tryall ID
        let finalUrlString = urlString + id
        
        //Make AlamoFireRequest
        Alamofire.request(finalUrlString).responseJSON { response in
            if let jsonData = response.data {
                let result = self.processTrialsRequest(data: jsonData, error: response.result as? Error)
                OperationQueue.main.addOperation {
                    completion(result)
                }
            }
        }
    }
    
    func fetchAllBroadCategories(completion: @escaping (CategoriesResults) -> Void) {
        //Get url value
        let urlString = TryallAPI.retrieveBroadCategories.absoluteString
        
        //Make AlamoFireRequest
        Alamofire.request(urlString).responseJSON { response in
            if let jsonData = response.data {
                let result = self.processCategoriesRequest(data: jsonData, error: response.result as? Error)
                OperationQueue.main.addOperation {
                    completion(result)
                }
            }
        }
    }
    
    func fetch10RandomBroadCategories(completion: @escaping (CategoriesResults) -> Void) {
        //Get url value
        let urlString = TryallAPI.retrieveRandom10BroadCategories.absoluteString
        
        //Make AlamoFireRequest
        Alamofire.request(urlString).responseJSON { response in
            if let jsonData = response.data {
                let result = self.processCategoriesRequest(data: jsonData, error: response.result as? Error)
                OperationQueue.main.addOperation {
                    completion(result)
                }
            }
        }
    }
    
    func fetchMyCategories(completion: @escaping (CategoriesResults) -> Void) {
        //Get url value
        let urlString = TryallAPI.retrieveMyCategories.absoluteString
        
        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
                let parameters: Parameters  = ["id_token":idToken!]
                //Make AlamoFireRequest
                Alamofire.request(urlString, parameters: parameters).responseJSON { response in
                    if let jsonData = response.data {
                        let result = self.processCategoriesRequest(data: jsonData, error: response.result as? Error)
                        
                        print(response.result.value)
                        OperationQueue.main.addOperation {
                            completion(result)
                        }
                    }
                }
            } else {
                print("user object unavailable")
            }
        }
        
        
    }
    
    func fetch10RecentlyAddedTrials(completion: @escaping (TrialsResults) -> Void) {
        //Get url value
        let urlString = TryallAPI.retrieve10RecentlyAddedTrials.absoluteString
        
        //Make AlamoFireRequest
        Alamofire.request(urlString).responseJSON { response in
            if let jsonData = response.data {
                let result = self.processTrialsRequest(data: jsonData, error: response.result as? Error)
                OperationQueue.main.addOperation {
                    completion(result)
                }
            }
        }
    }
    
    func removeFromSaved(id:String,completion: @escaping (Bool) -> Void) {
        //Get url value
        let urlString = TryallAPI.removeFromSaved.absoluteString
        //Append provided tryall ID
        let finalUrlString = urlString + id

        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
                let parameters: Parameters  = ["id_token":idToken!]
                        //Make AlamoFireRequest
                Alamofire.request(finalUrlString, method: .delete, parameters: parameters).responseJSON { response in
                    switch (response.result) {
                    case .success(_):
                        completion(true)
                    case .failure(_):
                        completion(false)
                    }
                }
            } else {
                print("user object unavailable")
            }
        }
    }
    
    func addToSaved(trialID:String,completion: @escaping (Bool) -> Void) {
        //Get url value
        let urlString = TryallAPI.removeFromSaved.absoluteString
        
        //Append provided tryall ID
        let finalUrlString = urlString + trialID
        
        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
                let newString = finalUrlString + "?id_token=\(idToken!)"
                //Make AlamoFireRequest
                Alamofire.request(newString, method: .post).responseJSON { response in
                    switch (response.result) {
                    case .success(_):
                        completion(true)
                    case .failure(_):
                        completion(false)
                    }
                }
            } else {
                print("user object unavailable")
            }
        }
        
    }

    
    func fetchGeneralTrials(completion: @escaping (TrialsResults) -> Void) {
        //Get url value
        let url = TryallAPI.retrieveGeneralTrials
        
        //Make AlamoFireRequest
        Alamofire.request(url).responseJSON { response in
            if let jsonData = response.data {
                let result = self.processTrialsRequest(data: jsonData, error: response.result as? Error)
                OperationQueue.main.addOperation {
                    completion(result)
                }
            }
        }
    }
    func fetchActiveTrials(completion: @escaping (TrialsResults) -> Void) {
        //Get url value
        let url = TryallAPI.retrieveActiveTrials
        
        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
                let parameters: Parameters  = ["id_token":idToken!]
                
                //Make AlamoFireRequest
                Alamofire.request(url,parameters: parameters).responseJSON { response in
                    if let jsonData = response.data {
                        let result = self.processTrialsRequest(data: jsonData, error: response.result as? Error)
                        OperationQueue.main.addOperation {
                            completion(result)
                        }
                    }
                }
            } else {
                print("user object unavailable")
            }
        }
    }
    func fetchSavedTrials(completion: @escaping (TrialsResults) -> Void) {
        //Get url value
        let url = TryallAPI.retrieveSavedTrials
        
        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
                let parameters: Parameters  = ["id_token":idToken!]
                
                //Make AlamoFireRequest
                Alamofire.request(url,parameters: parameters).responseJSON { response in
                    if let jsonData = response.data {
                        let result = self.processTrialsRequest(data: jsonData, error: response.result as? Error)
                        OperationQueue.main.addOperation {
                            completion(result)
                            }
                        }
                    }
                } else {
                print("user object unavailable")
                }
            }
        }
    func fetchPastTrials(completion: @escaping (TrialsResults) -> Void) {
        //Get url value
        let url = TryallAPI.retrievePastTrials
        
        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
                let parameters: Parameters  = ["id_token":idToken!]
                
                //Make AlamoFireRequest
                Alamofire.request(url,parameters: parameters).responseJSON { response in
                    if let jsonData = response.data {
                        let result = self.processTrialsRequest(data: jsonData, error: response.result as? Error)
                        OperationQueue.main.addOperation {
                            completion(result)
                        }
                    }
                }
            } else {
                print("user object unavailable")
            }
        }
    }
    
    func saveDeviceToken(deviceToken:String,completion: @escaping (Bool) -> Void) {
        //Get url value
        let urlString = TryallAPI.saveNotificationToken.absoluteString
        
        //Append provided tryall ID
        let finalUrlString = urlString + deviceToken
        
        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
                let newString = finalUrlString + "?id_token=\(idToken!)"
                //Make AlamoFireRequest
                Alamofire.request(newString, method: .post).responseJSON { response in
                    switch (response.result) {
                    case .success(_):
                        completion(true)
                    case .failure(_):
                        completion(false)
                    }
                }
            } else {
                print("user object unavailable")
            }
        }
        
    }
    func saveFrequencySelection(highFrequency:Bool,completion: @escaping (Bool) -> Void) {
        //Get url value
        let urlString = TryallAPI.postHighFrequencyValue.absoluteString
        
        //Append provided tryall ID
        let finalUrlString = urlString + String(highFrequency)
        
        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
                let newString = finalUrlString + "?id_token=\(idToken!)"
                //Make AlamoFireRequest
                Alamofire.request(newString, method: .post).responseJSON { response in
                    switch (response.result) {
                    case .success(_):
                        completion(true)
                    case .failure(_):
                        completion(false)
                    }
                }
            } else {
                print("user object unavailable")
            }
        }
        
    }
    func postSevenDayReminder(sevenDayVal:Bool,completion: @escaping (Bool) -> Void) {
        //Get url value
        let urlString = TryallAPI.saveSevenDayVal.absoluteString
        
        //Append provided tryall ID
        let finalUrlString = urlString + String(sevenDayVal)
        
        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
                let newString = finalUrlString + "?id_token=\(idToken!)"
                //Make AlamoFireRequest
                Alamofire.request(newString, method: .post).responseJSON { response in
                    switch (response.result) {
                    case .success(_):
                        completion(true)
                    case .failure(_):
                        completion(false)
                    }
                }
            } else {
                print("user object unavailable")
            }
        }
        
    }
    func postFiveDayReminder(fiveDayVal:Bool,completion: @escaping (Bool) -> Void) {
        //Get url value
        let urlString = TryallAPI.saveFiveDayVal.absoluteString
        
        //Append provided tryall ID
        let finalUrlString = urlString + String(fiveDayVal)
        
        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
                let newString = finalUrlString + "?id_token=\(idToken!)"
                //Make AlamoFireRequest
                Alamofire.request(newString, method: .post).responseJSON { response in
                    switch (response.result) {
                    case .success(_):
                        completion(true)
                    case .failure(_):
                        completion(false)
                    }
                }
            } else {
                print("user object unavailable")
            }
        }
    }
    func postThreeDayReminder(threeDayVal:Bool,completion: @escaping (Bool) -> Void) {
        //Get url value
        let urlString = TryallAPI.saveThreeDayVal.absoluteString
        
        //Append provided tryall ID
        let finalUrlString = urlString + String(threeDayVal)
        
        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
                let newString = finalUrlString + "?id_token=\(idToken!)"
                //Make AlamoFireRequest
                Alamofire.request(newString, method: .post).responseJSON { response in
                    switch (response.result) {
                    case .success(_):
                        completion(true)
                    case .failure(_):
                        completion(false)
                    }
                }
            } else {
                print("user object unavailable")
            }
        }
    }
    func postOneDayReminder(oneDayVal:Bool,completion: @escaping (Bool) -> Void) {
        //Get url value
        let urlString = TryallAPI.saveOneDayVal.absoluteString
        
        //Append provided tryall ID
        let finalUrlString = urlString + String(oneDayVal)
        
        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
                let newString = finalUrlString + "?id_token=\(idToken!)"
                //Make AlamoFireRequest
                Alamofire.request(newString, method: .post).responseJSON { response in
                    switch (response.result) {
                    case .success(_):
                        completion(true)
                    case .failure(_):
                        completion(false)
                    }
                }
            } else {
                print("user object unavailable")
            }
        }
    }
    
    func fetchFrequencyVal(completion: @escaping (Bool) -> Void) {
        //Get url value
        let url = TryallAPI.retrieveHighFrequencyValue
        
        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
                let parameters: Parameters  = ["id_token":idToken!]
                
                //Make AlamoFireRequest
                Alamofire.request(url,parameters: parameters).responseJSON { response in
                    if let jsonData = response.data {
                        let result = self.processFrequencyRequest(data: jsonData, error: response.result as? Error)
                        OperationQueue.main.addOperation {
                            completion(result)
                        }
                    }
                }
            } else {
                print("user object unavailable")
            }
        }
    }
    func fetchSevenDayReminderVal(completion: @escaping (Bool) -> Void) {
        //Get url value
        let url = TryallAPI.retrievegeneralRemindersVal
        
        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
                let parameters: Parameters  = ["id_token":idToken!]
                
                //Make AlamoFireRequest
                Alamofire.request(url,parameters: parameters).responseJSON { response in
                    if let jsonData = response.data {
                        let result = self.processRemindersRequest(specificVal: "sevenDays", data: jsonData, error: response.result as? Error)
                        OperationQueue.main.addOperation {
                            completion(result)
                        }
                    }
                }
            } else {
                print("user object unavailable")
            }
        }
    }
    func fetchFiveDayReminderVal(completion: @escaping (Bool) -> Void) {
        //Get url value
        let url = TryallAPI.retrievegeneralRemindersVal
        
        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
                let parameters: Parameters  = ["id_token":idToken!]
                
                //Make AlamoFireRequest
                Alamofire.request(url,parameters: parameters).responseJSON { response in
                    if let jsonData = response.data {
                        let result = self.processRemindersRequest(specificVal: "fiveDays", data: jsonData, error: response.result as? Error)
                        OperationQueue.main.addOperation {
                            completion(result)
                        }
                    }
                }
            } else {
                print("user object unavailable")
            }
        }
    }
    func fetchThreeDayReminderVal(completion: @escaping (Bool) -> Void) {
        //Get url value
        let url = TryallAPI.retrievegeneralRemindersVal
        
        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
                let parameters: Parameters  = ["id_token":idToken!]
                
                //Make AlamoFireRequest
                Alamofire.request(url,parameters: parameters).responseJSON { response in
                    if let jsonData = response.data {
                        let result = self.processRemindersRequest(specificVal: "threeDays", data: jsonData, error: response.result as? Error)
                        OperationQueue.main.addOperation {
                            completion(result)
                        }
                    }
                }
            } else {
                print("user object unavailable")
            }
        }
    }
    func fetchOneDayReminderVal(completion: @escaping (Bool) -> Void) {
        //Get url value
        let url = TryallAPI.retrievegeneralRemindersVal
        
        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
                let parameters: Parameters  = ["id_token":idToken!]
                
                //Make AlamoFireRequest
                Alamofire.request(url,parameters: parameters).responseJSON { response in
                    if let jsonData = response.data {
                        let result = self.processRemindersRequest(specificVal: "oneDay", data: jsonData, error: response.result as? Error)
                        OperationQueue.main.addOperation {
                            completion(result)
                        }
                    }
                }
            } else {
                print("user object unavailable")
            }
        }
    }
    
    
}
