//
//  TryallAPI.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 11/14/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import Foundation

enum trialRoutes: String {
    case activeTrials = "/api/trials/active"
    
    //Saved Trials Routes
    case savedTrials = "/api/trials/saved"
    case removeFromSaved = "/api/trials/saved/"
    
    //Past Trial Routes
    case pastTrials = "/api/trials/past"
    
    // Retrieve General homefeed trials
    case retrieveTrials = "/api/trials"
    
    //RetrieveSingleTrial
    case retrieveSingleTrial = "/api/trials/"
    
    //Update Notification Token
    case saveNotificationToken = "/api/profile/"
    
    //Update High frequency Setting
    case highFrequencySchedule = "/api/profile/frequency/"
    
    //Retrieve High Frequency Setting
    case highFrequencyRetrieval = "/api/profile/frequency"
    
    //Post Changing reminder value
    case sevenDayReminderSave = "/api/profile/reminders/sevenDays/"
    case fiveDayReminderSave = "/api/profile/reminders/fiveDays/"
    case threeDayReminderSave = "/api/profile/reminders/threeDays/"
    case oneDayReminderSave = "/api/profile/reminders/oneDay/"
    
    
    //Retrieve Reminder Vals
    case generalReminderRetrieval = "/api/profile/reminders"

    
    // Retrieve similar trials
    case similarTrials = "/api/similar/"
    
    // Retrieve all broad categories
    case broadCategories = "/all-categories"
    
    // Retrieve 10 random broad categories
    case random10broadCategories = "/categories"
    
    // Retrieve 10 recently added trials
    case tenRecentlyAddedTrials = "/api/recently-added"
    
    // Search
    case search = "/search"
    
    // My categories
    case myCategories = "/api/my-categories"

}

struct TryallAPI {
    //private static let baseURLString = "http://192.168.1.5:8000"
    private static let baseSearchString = "http://34.229.128.48:8000"
    private static let baseURLString = "https://infinite-cliffs-47750.herokuapp.com"

    
    //List of separate URLS
    static var retrieveGeneralTrials:URL {
        return tryallURL(trialRoute: .retrieveTrials)
    }
    static var retrieveSavedTrials:URL {
        return tryallURL(trialRoute: .savedTrials)
    }
    static var retrieveActiveTrials:URL {
        return tryallURL(trialRoute: .activeTrials)
    }
    static var retrievePastTrials:URL {
        return tryallURL(trialRoute: .pastTrials)
    }
    static var retrieveSingleTrial:URL {
        return tryallURL(trialRoute: .retrieveSingleTrial)
    }
    static var removeFromSaved:URL {
         return tryallURL(trialRoute: .removeFromSaved)
    }
    static var saveNotificationToken:URL {
        return tryallURL(trialRoute: .saveNotificationToken)
    }
    static var postHighFrequencyValue:URL {
        return tryallURL(trialRoute: .highFrequencySchedule)
    }
    static var retrieveHighFrequencyValue:URL {
        return tryallURL(trialRoute: .highFrequencyRetrieval)
    }
    static var saveSevenDayVal:URL {
        return tryallURL(trialRoute: .sevenDayReminderSave)
    }
    static var retrievegeneralRemindersVal:URL {
        return tryallURL(trialRoute: .generalReminderRetrieval)
    }

    static var saveFiveDayVal:URL {
        return tryallURL(trialRoute: .fiveDayReminderSave)
    }
    static var saveThreeDayVal:URL {
        return tryallURL(trialRoute: .threeDayReminderSave)
    }
    static var saveOneDayVal:URL {
        return tryallURL(trialRoute: .oneDayReminderSave)
    }
    static var retrieveSimilarTrials:URL {
        return tryallURL(trialRoute: .similarTrials)
    }
    static var retrieveBroadCategories:URL {
        return tryallURL(trialRoute: .broadCategories)
    }
    static var retrieveRandom10BroadCategories:URL {
        return tryallSearchURL(trialRoute: .random10broadCategories)
    }
    static var retrieve10RecentlyAddedTrials:URL {
        return tryallSearchURL(trialRoute: .tenRecentlyAddedTrials)
    }
    static var retrieveSearchResults:URL {
        return tryallSearchURL(trialRoute: .search)
    }
    static var retrieveMyCategories:URL {
        return tryallURL(trialRoute: .myCategories)
    }
    

    
    //Function to output a single URL to be used by AlamoFire
    private static func tryallURL(trialRoute: trialRoutes) -> URL {
        //Components of request
        let components = URLComponents(string: baseURLString + trialRoute.rawValue)!
        
        //Additional Router logic if need be
        return components.url!
    }
    private static func tryallSearchURL(trialRoute: trialRoutes) -> URL {
        //Components of request
        let components = URLComponents(string: baseSearchString + trialRoute.rawValue)!
        
        //Additional Router logic if need be
        return components.url!
    }
    
    static func frequencyVal(fromJSON data: Data) -> Bool? {
        
        do {
            //Create json Object array from data parameter
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
            guard let frequencyArrayObj = jsonObject as? [[String: Any]] else {
               return nil
            }
            guard let highFrequency = frequencyArrayObj[0]["highFrequency"] as? Int
                else {
                    return nil
            }
            switch (highFrequency) {
            case 1:
                return true
            default:
                return false
            }
        } catch let error{
            return nil
        }
    }
    static func reminderVal(specificVal:String,fromJSON data: Data) -> Bool? {
        
        do {
            //Create json Object array from data parameter
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
            guard let reminderArrayObj = jsonObject as? [[String:[String:Any]]] else {
                return nil
            }
            guard let reminderVal = reminderArrayObj[0]["preferences"]
                else {
                    return nil
            }
            guard let reminderValFinal = reminderVal[specificVal] as? Int
                else {
                    return nil
            }
            switch (reminderValFinal) {
            case 1:
                return true
            default:
                return false
            }
        } catch let error{
            return nil
        }
    }
        
    //Function to take in JSON Object and return single Trial object
    static func trial(fromJSON json: [String: Any]) -> Trial? {
        guard let tryallID = json["_id"] as? String,
        let name = json["name"] as? String,
        let tagLine = json["tagLine"] as? String,
        let description = json["description"] as? String,
        let websiteLinkString = json["websiteLink"] as? String,
            let websiteLink = URL(string:websiteLinkString),
        let freeTrialPageString = json["freeTrialPage"] as? String,
            let freeTrialPage = URL(string:freeTrialPageString),
        let DurationInDays = json["DurationInDays"] as? Int,
        let MonthlyDollarPrice = json["monthlyDollarPrice"] as? Int,
        let logoLinkString = json["logoLink"] as? String,
            let logoLink = URL(string:logoLinkString)
            else {
                return nil
        }
        
        if let similar  = json["similar"] as? [[String: Any]] {
            return Trial(tryallID: tryallID, name: name, tagLine: tagLine, description: description, websiteLink: websiteLink, freeTrialPage: freeTrialPage, DurationInDays: DurationInDays, MonthlyDollarPrice: MonthlyDollarPrice, logoLink: logoLink, similar: similar)
        }
        return Trial(tryallID: tryallID, name: name, tagLine: tagLine, description: description, websiteLink: websiteLink, freeTrialPage: freeTrialPage, DurationInDays: DurationInDays, MonthlyDollarPrice: MonthlyDollarPrice, logoLink: logoLink)
    }
    
    private static func category(fromJSON json: [String: Any]) -> Interest? {
        guard let id = json["_id"] as? String,
            let name = json["name"] as? String
            else{
                return nil
        }
        
        return Interest(id: id, name: name)
    }
    
    private static func search(fromJSON json: [String: Any]) -> SearchResult? {
        return SearchResult(data: json)
    }
    
    //Function to return Array of Trials
    static func trials(fromJSON data: Data) -> TrialsResults {
        do {
            //Create json Object array from data parameter
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
            guard let trialArray = jsonObject as? [[String: Any]] else {
                return .failure(TrialError.invalidJSONData)
            }
            //Create Array of Trials
            var finalTrials = [Trial]()
            
            //Loop through trialArray and create trial objects from json trials
            for trialJSON in trialArray {
                if let trial = trial(fromJSON: trialJSON) {
                    finalTrials.append(trial)
                }
            }
            //Unable to append Trial objects but did get JSON Data back
            if finalTrials.isEmpty && !trialArray.isEmpty {
                return .failure(TrialError.invalidJSONData)
            }
            
            //Return successful result with final Trials Array
            return .success(finalTrials)
            
        } catch let error {
            //return error if fails
            return .failure(error)
        }
    }
    
    static func categories(fromJSON data: Data) -> CategoriesResults {
        do {
            //Create json Object array from data parameter
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
            guard let categoryArray = jsonObject as? [[String: Any]] else {
                return .failure(CategoryError.invalidJSONData)
            }
            //Create Array of Interests / Categories
            var finalCategories = [Interest]()
        
            for categoryJSON in categoryArray {
                if let category =  category(fromJSON: categoryJSON) {
                    finalCategories.append(category)
                }
            }
         
            if finalCategories.isEmpty && !finalCategories.isEmpty {
                return .failure(CategoryError.invalidJSONData)
            }
            
            return .success(finalCategories)
            
        } catch let error {
            //return error if fails
            return .failure(error)
        }
    }
    
    
    static func searches(fromJSON data: Data) -> SearchResults {
        do {
            //Create json Object array from data parameter
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
            guard let searchesArray = jsonObject as? [[String: Any]] else {
                return .failure(SearchError.invalidJSONData)
            }
            //Create Array of Interests / Categories
            var finalSearchResults = [SearchResult]()
            
            for searchItem in searchesArray {
                if let searchResult =  search(fromJSON: searchItem) {
                    finalSearchResults.append(searchResult)
                }
            }
            
            if finalSearchResults.isEmpty && !finalSearchResults.isEmpty {
                return .failure(SearchError.invalidJSONData)
            }
            
            return .success(finalSearchResults)
            
        } catch let error {
            //return error if fails
            return .failure(error)
        }
    }
    
}

