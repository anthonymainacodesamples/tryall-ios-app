//
//  Trial.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 11/14/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import Foundation


class Trial {
    let tryallID: String
    let name: String
    let tagLine: String
    let description: String
    let websiteLink: URL
    let freeTrialPage: URL
    let DurationInDays: Int
    let MonthlyDollarPrice: Int
    let logoLink:URL
    var similar: [Trial] = []
    
    init(tryallID: String, name: String, tagLine: String, description: String, websiteLink: URL,freeTrialPage: URL,DurationInDays: Int, MonthlyDollarPrice: Int,logoLink:URL) {
        self.tryallID = tryallID
        self.name = name
        self.tagLine = tagLine
        self.description = description
        self.websiteLink = websiteLink
        self.freeTrialPage = freeTrialPage
        self.DurationInDays = DurationInDays
        self.MonthlyDollarPrice = MonthlyDollarPrice
        self.logoLink = logoLink
        
    }
    
    init(tryallID: String, name: String, tagLine: String, description: String, websiteLink: URL,freeTrialPage: URL,DurationInDays: Int, MonthlyDollarPrice: Int,logoLink:URL, similar: [[String: Any]]) {
        self.tryallID = tryallID
        self.name = name
        self.tagLine = tagLine
        self.description = description
        self.websiteLink = websiteLink
        self.freeTrialPage = freeTrialPage
        self.DurationInDays = DurationInDays
        self.MonthlyDollarPrice = MonthlyDollarPrice
        self.logoLink = logoLink
        
        for item in similar {
            let x = TryallAPI.trial(fromJSON: item)
            self.similar.append(x!)
        }
        
    }
}
