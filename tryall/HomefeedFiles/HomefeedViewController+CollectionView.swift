//
//  HomefeedViewController+CollectionView.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 11/16/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit
//
//extension HomefeedViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
//
//    // MARK: - Configuration
//
//    internal func configure(collectionView: UICollectionView) {
//        //Register Cells we'll be reusing
//        collectionView.registerReusableCell(SingleTrialCell.self)
//        collectionView.registerSupplementaryView(FeaturedheaderCollectionReusableView.self, kind: UICollectionElementKindSectionHeader)
//
//        //Set Data Source and Delegate
//        collectionView.dataSource = self
//        collectionView.delegate = self
//
//        //Place insets for content
//        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
//    }
//
//    // MARK: - UICollectionViewDataSource
//
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 3
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        return SingleTrialCell.dequeue(fromCollectionView: collectionView, atIndexPath:indexPath)
//    }
//
//    // MARK: - UICollectionViewDelegateFlowLayout
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        if UIDevice.current.userInterfaceIdiom == .phone {
//            return CGSize(width: collectionView.bounds.width, height: BaseRoundedCardCell.cellHeight)
//        } else {
//
//            //iPhone Attributes
//
//
//            // Number of Items per Row
//            let numberOfItemsInRow = 2
//
//            // Current Row Number
//            let rowNumber = indexPath.item/numberOfItemsInRow
//
//            // Compressed With
//            let compressedWidth = collectionView.bounds.width/3
//
//            // Expanded Width
//            let expandedWidth = (collectionView.bounds.width/3) * 2
//
//            // Is Even Row
//            let isEvenRow = rowNumber % 2 == 0
//
//            // Is First Item in Row
//            let isFirstItem = indexPath.item % numberOfItemsInRow != 0
//
//            // Calculate Width
//            var width: CGFloat = 0.0
//            if isEvenRow {
//                width = isFirstItem ? compressedWidth : expandedWidth
//            } else {
//                width = isFirstItem ? expandedWidth : compressedWidth
//            }
//
//            return CGSize(width: width, height: BaseRoundedCardCell.cellHeight)
//        }
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: collectionView.bounds.width, height: FeaturedheaderCollectionReusableView.viewHeight)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        let sectionHeader = FeaturedheaderCollectionReusableView.dequeue(fromCollectionView: collectionView, ofKind: kind, atIndexPath: indexPath)
//        return sectionHeader
//    }
//
//    // MARK: - UICollectionViewDelegate
//
////    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
////        if let cell = collectionView.cellForItem(at: indexPath) {
////            presentStoryAnimationController.selectedCardFrame = cell.frame
////            dismissStoryAnimationController.selectedCardFrame = cell.frame
////            performSegue(withIdentifier: "presentStory", sender: self)
////        }
////    }
//
//
//}

