//
//  TrialDisplayHeaderView.swift
//  tryall
//
//  Created by Gabriel Wamunyu on 11/25/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import Foundation
import UIKit

class TrialDisplayHeaderView: UITableViewCell {
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var tagLineLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    
}
