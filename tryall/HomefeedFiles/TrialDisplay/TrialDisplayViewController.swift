//
//  TrialDisplayViewController.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 11/19/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit

class TrialDisplayViewController: UITableViewController {

    
    
    //Initialize values and source classes
    var trialStore: TrialStore! = TrialStore()
    var trialsToDisplay: [Trial] = []
    
    
    var headerTrial: Trial?
    var selectedTrialID : String!
    
    private let firstTrialCellHeight = CGFloat(479)
    private let similarTrialsCellHeight = CGFloat(129)
    
    var isSaved : Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.updateRightBarButton(isSaved: self.isSaved)
        
        //Add Bar Button Item
        //self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"saveIcon"), style: .plain, target: self, action: #selector(saveTapped))
        
        self.navigationController?.navigationBar.tintColor = PRIMARY_APP_BLUE
        tableView.register(UINib(nibName: "TrialDisplayHeaderView", bundle: nil), forCellReuseIdentifier: "trialDisplayHeader")
        tableView.register(UINib(nibName: "TrialDisplaySimilarTableViewCell", bundle: nil), forCellReuseIdentifier: "trialDisplaySimilar")
        fetchTrialInfo()
        fetchSimilarTrials()
    }
    
    func fetchSimilarTrials(){
        trialStore.fetchSimilarTrials(id: selectedTrialID!) { (trialsResult) in
            switch (trialsResult) {
            case let .success(trials):
                self.trialsToDisplay = trials
                print("Success: \(trials.count)")
                self.tableView.reloadData()
            case let .failure(error):
                print("Similar trials not found: \(error)")
                
            }
        }
    }
    
    func fetchTrialInfo(){
        //General non-auth test for data retrieval
        trialStore.fetchSingleTrial(id: selectedTrialID!) { (trialsResult) in
            switch (trialsResult) {
            case let .success(trials):
                self.headerTrial = trials[0]
                self.tableView.reloadData()
            case let .failure(error):
                print("Single trial not found: \(error)")
                
            }
        }
    }
    
    @objc func saveTapped(_ sender: Any) {
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if self.trialsToDisplay.count > 0 {
            return 2
        }
        return 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 { 
            return "Similar Trials"
        }
        return ""
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "trialDisplayHeader") as! TrialDisplayHeaderView
    
            let trial = self.headerTrial
            cell.logoImageView.pin_setImage(from: trial!.logoLink)
            
            //Title for nav bar
            self.navigationItem.title = trial!.name
            
            cell.tagLineLabel.text = trial!.tagLine
            cell.descriptionLabel.text = trial!.description
            cell.costLabel.text = String(trial!.MonthlyDollarPrice)
            cell.durationLabel.text = String(trial!.DurationInDays)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "trialDisplaySimilar", for: indexPath) as! TrialDisplaySimilarTableCellView
            let trial = self.trialsToDisplay[indexPath.row]
            cell.lblName.text = trial.name
            cell.logoImg.pin_setImage(from: trial.logoLink)
            cell.lblCost.text = "\(trial.MonthlyDollarPrice) / month"
            cell.lblDuration.text = "\(trial.DurationInDays) days"
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "trialDisplay") as! TrialDisplayViewController
            newViewController.selectedTrialID = self.trialsToDisplay[indexPath.row].tryallID
            self.navigationController?.pushViewController(newViewController, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return self.firstTrialCellHeight
        }
        return self.similarTrialsCellHeight
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            if (self.headerTrial != nil) {
                return 1
            }
            return 0
        }
        
        return self.trialsToDisplay.count
    }
    func updateRightBarButton(isSaved : Bool){
        
        let btnSave = UIButton(frame: CGRect(x:0,y:0,width:30,height:30))
        btnSave.addTarget(self, action: #selector(TrialDisplayViewController.btnSaveDidTap), for: .touchUpInside)
        
        if isSaved{
            btnSave.setImage(UIImage(named: "savedIcon"), for: .normal)
        }else{
            btnSave.setImage(UIImage(named: "saveIcon"), for:.normal)
        }
        let rightButton = UIBarButtonItem(customView: btnSave)
        self.navigationItem.setRightBarButtonItems([rightButton], animated: true)
    }
    @objc func btnSaveDidTap()
    {
        //do your stuff
        self.isSaved = !self.isSaved;
        if self.isSaved {
            self.save();
        }else{
            self.unsave();
        }
        self.updateRightBarButton(isSaved: self.isSaved);
    }
    func save()
    {
        //do your favourite stuff/logic
        trialStore.addToSaved(trialID: self.selectedTrialID!) { (boolVal) in
            switch (boolVal) {
            case true:
                print("Success in Saving")
            case false:
                print("Unsuccessul in Saving")
            }
        }
    }
    func unsave(){
        //do your unfavourite logic
        trialStore.removeFromSaved(id: self.selectedTrialID!) { (boolVal) in
            switch (boolVal) {
            case true:
                print("Success in Unsaving")
            case false:
                print("Unsuccessul in unsaving")
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
