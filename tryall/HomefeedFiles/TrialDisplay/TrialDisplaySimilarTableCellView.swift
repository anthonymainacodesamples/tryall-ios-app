//
//  TrialDisplaySimilarTableCellView.swift
//  tryall
//
//  Created by Gabriel Wamunyu on 11/26/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit

class TrialDisplaySimilarTableCellView: UITableViewCell {

    @IBOutlet var logoImg: UIImageView!
    @IBOutlet var lblCost: UILabel!
    @IBOutlet var lblDuration: UILabel!
    @IBOutlet var lblName: UILabel!
}
