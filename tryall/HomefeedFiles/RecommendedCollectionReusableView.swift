//
//  RecommendedCollectionReusableView.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 11/17/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit

class RecommendedCollectionReusableView: UICollectionReusableView {
    
    internal static let viewHeight: CGFloat = 60

    // MARK: - Factory Method
    
    internal static func dequeue(fromCollectionView collectionView: UICollectionView, ofKind kind: String, atIndexPath indexPath: IndexPath) -> RecommendedCollectionReusableView {
        guard let view: RecommendedCollectionReusableView = collectionView.dequeueSupplementaryView(kind: kind, indexPath: indexPath) else {
            fatalError("*** Failed to dequeue RecommendedSectionHeader ***")
        }
        return view
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
