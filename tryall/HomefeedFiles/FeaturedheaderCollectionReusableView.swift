//
//  FeaturedheaderCollectionReusableView.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 11/15/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit

class FeaturedheaderCollectionReusableView: UICollectionReusableView {
    
    internal static let viewHeight: CGFloat = 60
    
    
    // MARK: - Factory Method
    
    internal static func dequeue(fromCollectionView collectionView: UICollectionView, ofKind kind: String, atIndexPath indexPath: IndexPath) -> FeaturedheaderCollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FeaturedheaderCollectionReusableView", for: indexPath)
        
        return view as! FeaturedheaderCollectionReusableView
    }
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
