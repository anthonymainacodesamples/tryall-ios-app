//
//  HomefeedViewController.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 11/16/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit
import ViewAnimator

class HomefeedViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    //Initialize values and source classes
    var trialStore: TrialStore!
    var trialsToDisplay: [Trial] = []
    
    //Segue Transport
    
    var selectedTrialID:String?

    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        //Register Cells we'll be reusing
        collectionView.registerReusableCell(SingleTrialCell.self)
        collectionView.registerSupplementaryView(FeaturedheaderCollectionReusableView.self, kind: UICollectionElementKindSectionHeader)
        collectionView.registerSupplementaryView(RecommendedCollectionReusableView.self, kind: UICollectionElementKindSectionHeader)
        
        //Set Data Source and Delegate
        collectionView.dataSource = self
        collectionView.delegate = self
        
        //ViewAnimator Stuff
        let fromAnimation = AnimationType.from(direction: .bottom, offset: 30.0)
        
        //Place insets for content
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        
        //General non-auth test for data retrieval
        trialStore = TrialStore()
        trialStore.fetchGeneralTrials { (trialsResult) in
            switch trialsResult {
                case let .success(trials):
                    self.trialsToDisplay = trials
                        print("Success")
                case let .failure(error):
                        print("No trials were loaded")
                }
            
                self.collectionView.reloadData()
                self.collectionView.animate(animations: [fromAnimation])
            
            }
        

        // Do any additional setup after loading the view.
         //configure(collectionView: collectionView)
        
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
            case 0:
                return self.trialsToDisplay.count
            case 1:
                return self.trialsToDisplay.count
            default:
                return 0
            
        }
            
    }
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        switch indexPath.section {
        case 0:
            let cell = SingleTrialCell.dequeue(fromCollectionView: collectionView, atIndexPath:indexPath)
            let trial = trialsToDisplay[indexPath.row]
            cell.tiralDescription.text = trial.description
            cell.trialDuration.text = String(trial.DurationInDays)
            cell.trialMonthlyCost.text = String(trial.MonthlyDollarPrice)
            
            //Logo Retrieval
            let logoUrl = trial.logoLink
            cell.updateImageView(with: logoUrl)
            
            return cell
            
        case 1:
            let cell = SingleTrialCell.dequeue(fromCollectionView: collectionView, atIndexPath:indexPath)
            let trial = trialsToDisplay[indexPath.row]
            cell.tiralDescription.text = trial.description
            cell.trialDuration.text = String(trial.DurationInDays)
            cell.trialMonthlyCost.text = String(trial.MonthlyDollarPrice)
            
            //Logo Retrieval
            let logoUrl = trial.logoLink
            cell.updateImageView(with: logoUrl)
            
            return cell
        default:
            let cell = SingleTrialCell.dequeue(fromCollectionView: collectionView, atIndexPath:indexPath)
            return cell
        }
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return CGSize(width: collectionView.bounds.width, height: BaseRoundedCardCell.cellHeight)
        } else {
            
            //iPhone Attributes
            
            
            // Number of Items per Row
            let numberOfItemsInRow = 2
            
            // Current Row Number
            let rowNumber = indexPath.item/numberOfItemsInRow
            
            // Compressed With
            let compressedWidth = collectionView.bounds.width/3
            
            // Expanded Width
            let expandedWidth = (collectionView.bounds.width/3) * 2
            
            // Is Even Row
            let isEvenRow = rowNumber % 2 == 0
            
            // Is First Item in Row
            let isFirstItem = indexPath.item % numberOfItemsInRow != 0
            
            // Calculate Width
            var width: CGFloat = 0.0
            if isEvenRow {
                width = isFirstItem ? compressedWidth : expandedWidth
            } else {
                width = isFirstItem ? expandedWidth : compressedWidth
            }
            
            return CGSize(width: width, height: BaseRoundedCardCell.cellHeight)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        switch section {
        case 0:
            return CGSize(width: collectionView.bounds.width, height: FeaturedheaderCollectionReusableView.viewHeight)
        case 1:
            return CGSize(width: collectionView.bounds.width, height: RecommendedCollectionReusableView.viewHeight)
        default:
            //TODO: Get better default return case
            return CGSize(width: collectionView.bounds.width, height: FeaturedheaderCollectionReusableView.viewHeight)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        
        switch indexPath.section {
        case 0:
            let featuredSectionHeader = FeaturedheaderCollectionReusableView.dequeue(fromCollectionView: collectionView, ofKind: kind, atIndexPath: indexPath)
            return featuredSectionHeader
        case 1:
            let recdSectionHeader = RecommendedCollectionReusableView.dequeue(fromCollectionView: collectionView, ofKind: kind, atIndexPath: indexPath)
            return recdSectionHeader
        default:
            let sectionHeader = FeaturedheaderCollectionReusableView.dequeue(fromCollectionView: collectionView, ofKind: kind, atIndexPath: indexPath)
            return sectionHeader
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            self.selectedTrialID = trialsToDisplay[indexPath.row].tryallID
            performSegue(withIdentifier: "presentTrial", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "presentTrial") {
            let vc = segue.destination as! TrialDisplayViewController
            vc.selectedTrialID = self.selectedTrialID
            }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
