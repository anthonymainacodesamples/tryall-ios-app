//
//  Interest.swift
//  tryall
//
//  Created by Gabriel Wamunyu on 11/20/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit

class InterestCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet var btnCategory: UIButton!
    @IBOutlet var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnCategory.titleLabel!.lineBreakMode = .byWordWrapping
        btnCategory.titleLabel!.textAlignment = .center
        btnCategory.layer.borderColor = UIColor.blue.cgColor
        btnCategory.layer.borderWidth = 1
        
        btnCategory.layer.cornerRadius = 22
        btnCategory.layer.masksToBounds = true
        
        

    }
}
