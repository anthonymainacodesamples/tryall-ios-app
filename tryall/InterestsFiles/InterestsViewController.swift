//
//  InterestsViewController.swift
//  tryall
//
//  Created by Gabriel Wamunyu on 11/20/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit
import Alamofire

class InterestsViewController: UICollectionViewController {
    
    var categories = [Interest]()
    var trialStore = TrialStore()
    var myCategories = [Interest]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.contentInset = UIEdgeInsets(top: 23, left: 16, bottom: 10, right: 16)
        collectionView?.register(UINib(nibName: "InterestCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "InterestCell")
        
        fetchCategories()
    }
    
    func fetchCategories(){
        
        
        
        Alamofire.request("http://34.229.128.48:8000/all-categories", encoding: JSONEncoding.default).responseJSON(completionHandler: { response in
            
            if(response.response?.statusCode == 404){
                print("Error retrieving search results")
                return
            }
            
            if let json =  response.result.value {
                
                if let categories = json as? [[String: Any]] {
                    
                    for item in categories {
                        let category = Interest(data: item)
                        self.categories.append(category)
                    }
                    
                    self.trialStore.fetchMyCategories(completion: { (categoriesResults) in
                        print("my categories: \(categoriesResults)")
                        
                        switch (categoriesResults) {
                        case let .success(vals):
                            if vals.count > 0 {
                                
                            }else{
                                print("no categories")
                            }
                            self.myCategories = vals
                            self.collectionView?.reloadData()
                        case let .failure(error):
                            print("Broad categories not found: \(error)")
                            
                        }
                    })
                   
                }else{
                    print("failed to parse categories")
                }
            }
        })
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = true
        }
    }
    
    //MARK: - CollectionView Delegate Methods
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categories.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! InterestCollectionViewCell
        let color = cell.btnCategory.backgroundColor
        
        if color == PRIMARY_APP_BLUE {
            cell.btnCategory.backgroundColor = UIColor.white
            cell.btnCategory.setTitleColor(PRIMARY_APP_BLUE, for: .normal)
        }else{
            cell.btnCategory.backgroundColor = PRIMARY_APP_BLUE
            cell.btnCategory.setTitleColor(UIColor.white, for: .normal)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InterestCell", for: indexPath as IndexPath) as! InterestCollectionViewCell
        cell.btnCategory.setTitle(self.categories[indexPath.row].name, for: .normal)
        cell.btnCategory.isUserInteractionEnabled = true
        
        for item in self.myCategories {
            if self.categories[indexPath.row].name == item.name {
                cell.btnCategory.backgroundColor = PRIMARY_APP_BLUE
                cell.btnCategory.setTitleColor(UIColor.white, for: .normal)
                break
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = (collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right + 10)) / 2
        return CGSize(width: itemSize, height: 80)
    }
}
