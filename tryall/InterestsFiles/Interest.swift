//
//  Interest.swift
//  tryall
//
//  Created by Gabriel Wamunyu on 11/20/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import Foundation

class Interest {
    
    var id: String
    var name: String
    
    init(data: [String: Any]) {
        
        self.id = data["_id"] as! String
        self.name = data["name"] as! String
        
    }
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}
