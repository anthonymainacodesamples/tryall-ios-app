//
//  AuthenticationViewController.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 11/11/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit
import Alamofire
import GoogleSignIn
import ViewAnimator

class AuthenticationViewController: UIViewController,GIDSignInUIDelegate {
    
    let googleSignInButton = GIDSignInButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        //adding the delegates
        GIDSignIn.sharedInstance().uiDelegate = self
        
        //getting the signin button and adding it to view
        googleSignInButton.style = GIDSignInButtonStyle.wide
        googleSignInButton.center.x = view.center.x
        googleSignInButton.center.y = view.center.y * 1.75
        view.addSubview(googleSignInButton)
        
        
        view.animateRandom()
        // [END_EXCLUDE]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        
        if(GIDSignIn.sharedInstance().hasAuthInKeychain()) {
        //Navigate to Right Page
        print("are we in here or nah")
        let storyBoard: UIStoryboard = UIStoryboard(name:"Main", bundle: Bundle.main)
        if let mainTabsController: UITabBarController = storyBoard.instantiateViewController(withIdentifier: "mainRoot") as? UITabBarController
            {
            self.present(mainTabsController, animated: true, completion: nil)
            }
        }
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                                  object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


