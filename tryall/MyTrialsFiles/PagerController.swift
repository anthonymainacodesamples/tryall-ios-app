//
//  PagerController.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 11/13/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit
import DTPagerController

class PagerController: DTPagerController {


    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedTextColor = PRIMARY_APP_BLUE

        
        //Strange navbar behavior fix
        self.navigationController?.navigationBar.isTranslucent = false
        
        //Storyboard instantiate
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        
        //Fix odd tab bar behavior
        self.extendedLayoutIncludesOpaqueBars = true
        self.edgesForExtendedLayout = .bottom

        
        let viewController1 = storyboard.instantiateViewController(withIdentifier: "activeTrialsController")
        viewController1.title = "Active"
//        viewController1.scrollView.backgroundColor = UIColor.green

        
        let viewController2 = storyboard.instantiateViewController(withIdentifier: "myTrialsGeneral")
        viewController2.title = "Saved"
//        viewController2.scrollView.backgroundColor = UIColor.purple

        
        let viewController3 = storyboard.instantiateViewController(withIdentifier: "pastTrialsController")
        viewController3.title = "Past"
//        viewController3.scrollView.backgroundColor = UIColor.red

        
        viewControllers = [viewController1, viewController2, viewController3]
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

