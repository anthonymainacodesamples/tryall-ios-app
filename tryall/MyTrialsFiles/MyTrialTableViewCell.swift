//
//  MyTrialTableViewCell.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 11/17/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit

class MyTrialTableViewCell: UITableViewCell {

    @IBOutlet weak var logoIMageView: UIImageView!
    @IBOutlet weak var trialTitleLabel: UILabel!
    @IBOutlet weak var durationOrCostLabel: UILabel!
    @IBOutlet weak var subTextLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateImageView(with logoUrl: URL) {
        // Creating a session object with the default configuration.
        // You can read more about it here https://developer.apple.com/reference/foundation/urlsessionconfiguration
        let session = URLSession(configuration: .default)
        
        // Define a download task. The download task will download the contents of the URL as a Data object and then you can do what you wish with that data.
        let downloadPicTask = session.dataTask(with: logoUrl) { (data, response, error) in
            // The download has finished.
            if let e = error {
                print("Error downloading logo \(e)")
            } else {
                // No errors found.
                // It would be weird if we didn't have a response, so check for that too.
                if let res = response as? HTTPURLResponse {
                    print("Downloaded logo with response code \(res.statusCode)")
                    if let imageData = data {
                        // Finally convert that Data into an image and do what you wish with it.
                        let image = UIImage(data: imageData)
                        // Do something with your image.
                        DispatchQueue.main.async { // Correct
                            self.logoIMageView.image = image
                        }
                    } else {
                        print("Couldn't get image: Image is nil")
                    }
                } else {
                    print("Couldn't get response code for some reason")
                }
            }
        }
        
        downloadPicTask.resume()
    }

}
