//
//  SavedTrialsTableViewController.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 11/13/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit
import ViewAnimator

class SavedTrialsTableViewController: UITableViewController {
    
    //Initialize values and source classes
    var trialStore: TrialStore!
    var trialsToDisplay: [Trial] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadSavedData()
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action:
            #selector(SavedTrialsTableViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl?.tintColor = PRIMARY_APP_BLUE
        self.tableView.addSubview(self.refreshControl!)
        
    }
    
     @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.loadSavedData()
        refreshControl.endRefreshing()
    }
    
    func loadSavedData() {
        let fromAnimation = AnimationType.from(direction: .right, offset: 30.0)

        //General non-auth test for data retrieval
        trialStore = TrialStore()
        trialStore.fetchSavedTrials { (trialsResult) in
            switch trialsResult {
            case let .success(trials):
                self.trialsToDisplay = trials
                print("Success in loading saved trials")
            case let .failure(error):
                print("No trials were loaded")
            }
            self.tableView.reloadData()
            self.tableView.animate(animations: [fromAnimation], duration: 0.5)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.trialsToDisplay.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myTrialsTableViewCell", for: indexPath) as! MyTrialTableViewCell

        let trial = trialsToDisplay[indexPath.row]
        cell.trialTitleLabel.text = trial.name
        cell.subTextLabel.text = trial.tagLine
        
        cell.durationOrCostLabel.text = String(trial.DurationInDays)
        //Logo Retrieval
        let logoUrl = trial.logoLink
        cell.updateImageView(with: logoUrl)

        return cell
    }


    /*
     MARK: - Navigation
     In a storyboard-based application, you will often want to do a little preparation before navigation
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "savedDetailsSegue") {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let vc = segue.destination as! TrialDisplayViewController
                let selectedTrialID = trialsToDisplay[indexPath.row].tryallID
                vc.selectedTrialID = selectedTrialID
                vc.isSaved = true
            }
        }
    }
    
    
 

}
