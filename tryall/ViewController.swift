//
//  ViewController.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 9/22/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit
import Alamofire
import GoogleSignIn


class ViewController: UIViewController {
    
    //Initialize values and source classes
    var trialStore: TrialStore!
    var trialsToDisplay: [Trial] = []
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
//        DispatchQueue.main.async {
//            GIDSignIn.sharedInstance().signInSilently()
//        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func testAuthClicked(_ sender: Any) {
        
        //General non-auth test for results
        trialStore = TrialStore()
        
//        trialStore.fetchGeneralTrials { (trialsResult) in
//            switch trialsResult {
//            case let .success(trials):
//                self.trialsToDisplay = trials
//                print("Success")
//            case let .failure(error):
//                print("No trials were loaded")
//            }
//        }
//

        trialStore.fetchSavedTrials { (trialsResult) in
            switch trialsResult {
            case let .success(trials):
                self.trialsToDisplay = trials
                print("Success in saved stuff yo")
            case let .failure(error):
                print("No saved trials were loaded")
            }
        }
        

        
//        if (GIDSignIn.sharedInstance().currentUser != nil) {
//            let idToken = GIDSignIn.sharedInstance().currentUser.authentication.idToken
////            print(idToken)
//
//            let parameters: Parameters  = [
//                "id_token":idToken
//            ]
//
            // Use accessToken in your URL Requests Header
//                Alamofire.request("http://192.168.1.5:8000/api/ios",method: .post,parameters: parameters,encoding: JSONEncoding.default).responseJSON { response in
//                        print("Request: \(String(describing: response.request))")   // original url request
//                        print("Response: \(String(describing: response.response))") // http url response
//                        print("Result: \(response.result)")                         // response serialization result
//
//                        if let json = response.result.value {
//                            print("JSON: \(json)") // serialized json response
//                        }
//
//                        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
//                            print("Data: \(utf8Text)") // original server data as UTF8 string
//                        }
//                    }
//            Alamofire.request("http://192.168.1.5:8000/api/trials").responseJSON { response in
//                print("Request: \(String(describing: response.request))")   // original url request
//                print("Response: \(String(describing: response.response))") // http url response
//                print("Result: \(response.result)")                         // response serialization result
//
//                if let json = response.result.value {
//                    print("JSON: \(json)") // serialized json response
//                }
//
//                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
//                    print("Data: \(utf8Text)") // original server data as UTF8 string
//                }
//            }
//        } else {
//            print("we still can't get it")
//        }
//
        

    }
    
}

