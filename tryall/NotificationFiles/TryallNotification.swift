//
//  Notification.swift
//  tryall
//
//  Created by Gabriel Wamunyu on 11/22/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import Foundation

//MARK: Types

struct PropertyKey {
    static let title = "title"
    static let body = "body"
    static let imageUrl = "imageUrl"
}


class TryallNotification: NSObject, NSCoding {
    
    //MARK: Archiving Paths
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("tryallnotifications")
    
    var title: String
    var body: String
    var imageUrl: String
    
    
    init(title: String, body: String, imageUrl: String) {
        self.title = title
        self.body = body
        self.imageUrl = imageUrl
    }
    
    //MARK: NSCoding
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard let title = aDecoder.decodeObject(forKey: PropertyKey.title) as? String else {
            print("Unable to decode the title for a Notification object.")
            return nil
        }
        guard let body = aDecoder.decodeObject(forKey: PropertyKey.body) as? String else {
            print("Unable to decode the body for a Notification object.")
            return nil
        }
        guard let imageUrl = aDecoder.decodeObject(forKey: PropertyKey.imageUrl) as? String else {
            print("Unable to decode the imageUrl for a Notification object.")
            return nil
        }
        self.init(title: title, body: body, imageUrl: imageUrl)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: PropertyKey.title)
        aCoder.encode(body, forKey: PropertyKey.body)
        aCoder.encode(imageUrl, forKey: PropertyKey.imageUrl)
        
    }
}
