//
//  NotificationCell.swift
//  tryall
//
//  Created by Gabriel Wamunyu on 11/22/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
  
    @IBOutlet var trialLogo: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var desc: UITextView!
    
}
