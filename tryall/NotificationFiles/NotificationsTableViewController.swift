//
//  NotificationsTableViewController.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 11/12/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit
import PINRemoteImage

class NotificationsTableViewController: UITableViewController {
    
    var notifications = [TryallNotification]()

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let res = self.loadNotifications() {
            print(res.count)
            self.notifications = res
        }else{
            print("savd notifications are empty")
        }
    }
    
    private func saveNotifications(){
        
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(self.notifications, toFile: TryallNotification.ArchiveURL.path)
        if isSuccessfulSave {
            print("Successfully saved notifications")
        }else{
            print("failed to save notifications")
        }
    }
    
    private func loadNotifications() -> [TryallNotification]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: TryallNotification.ArchiveURL.path) as? [TryallNotification]
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.notifications.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell

        cell.title.text = self.notifications[indexPath.row].title
        cell.desc.text = self.notifications[indexPath.row].body
        cell.trialLogo.pin_setImage(from: URL(string: self.notifications[indexPath.row].imageUrl))

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(88)
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            self.notifications.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.saveNotifications()
        }
    }
 

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
