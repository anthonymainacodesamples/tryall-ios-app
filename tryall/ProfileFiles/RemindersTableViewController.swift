//
//  RemindersTableViewController.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 11/12/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit

class RemindersTableViewController: UITableViewController {
    
    //Initialize values and source classes
    var trialStore: TrialStore! = TrialStore()

    @IBOutlet weak var sevenDaySwitch: UISwitch!
    @IBOutlet weak var fiveDaySwitch: UISwitch!
    @IBOutlet weak var threeDaySwitch: UISwitch!
    @IBOutlet weak var oneDaySwitch: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = PRIMARY_APP_BLUE
        
        sevenDaySwitch.addTarget(self, action: #selector(RemindersTableViewController.stateChanged(sender:)), for: UIControlEvents.valueChanged)
        fiveDaySwitch.addTarget(self, action: #selector(RemindersTableViewController.stateChanged(sender:)), for: UIControlEvents.valueChanged)
        threeDaySwitch.addTarget(self, action: #selector(RemindersTableViewController.stateChanged(sender:)), for: UIControlEvents.valueChanged)
        oneDaySwitch.addTarget(self, action: #selector(RemindersTableViewController.stateChanged(sender:)), for: UIControlEvents.valueChanged)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        retrieveReminderSelectionValue()
        
    }
    func retrieveReminderSelectionValue() {
        trialStore.fetchSevenDayReminderVal { (trialsResult) in
            switch trialsResult {
            case true:
                self.sevenDaySwitch.setOn(true, animated: false)
            case false:
                self.sevenDaySwitch.setOn(false, animated: false)
            }
        }
        trialStore.fetchFiveDayReminderVal { (trialsResult) in
            switch trialsResult {
            case true:
                self.fiveDaySwitch.setOn(true, animated: false)
            case false:
                self.fiveDaySwitch.setOn(false, animated: false)
            }
        }
        trialStore.fetchThreeDayReminderVal { (trialsResult) in
            switch trialsResult {
            case true:
                self.threeDaySwitch.setOn(true, animated: false)
            case false:
                self.threeDaySwitch.setOn(false, animated: false)
            }
        }
        trialStore.fetchOneDayReminderVal { (trialsResult) in
            switch trialsResult {
            case true:
                self.oneDaySwitch.setOn(true, animated: false)
            case false:
                self.oneDaySwitch.setOn(false, animated: false)
            }
        }
    }
    
    @objc func stateChanged(sender: UISwitch) {
        switch (sender) {
        case sevenDaySwitch:
            if sevenDaySwitch.isOn {
                trialStore.postSevenDayReminder(sevenDayVal: true) { (boolVal) in
                    switch (boolVal) {
                    case true:
                        print("Success in Saving Seven Day True")
                    case false:
                        print("Unsuccessul in saving Seven Day True")
                    }
                }
            } else {
                trialStore.postSevenDayReminder(sevenDayVal: false) { (boolVal) in
                    switch (boolVal) {
                    case true:
                        print("Success in Saving Seven Day False")
                    case false:
                        print("Unsuccessul in saving Seven Day False")
                    }
                }
            }
        case fiveDaySwitch:
            if fiveDaySwitch.isOn {
                trialStore.postFiveDayReminder(fiveDayVal: true) { (boolVal) in
                    switch (boolVal) {
                    case true:
                        print("Success in Saving Five Day True")
                    case false:
                        print("Unsuccessul in saving Five Day True")
                    }
                }
                
            } else {
                trialStore.postFiveDayReminder(fiveDayVal: false) { (boolVal) in
                    switch (boolVal) {
                    case true:
                        print("Success in Saving Five Day False")
                    case false:
                        print("Unsuccessul in saving Five Day False")
                    }
                }
            }
        case threeDaySwitch:
            if threeDaySwitch.isOn {
                trialStore.postThreeDayReminder(threeDayVal: true) { (boolVal) in
                    switch (boolVal) {
                    case true:
                        print("Success in Saving Three Day True")
                    case false:
                        print("Unsuccessul in saving Three Day True")
                    }
                }
            } else {
                trialStore.postThreeDayReminder(threeDayVal: false) { (boolVal) in
                    switch (boolVal) {
                    case true:
                        print("Success in Saving three Day False")
                    case false:
                        print("Unsuccessul in saving three Day False")
                    }
                }
            }
        case oneDaySwitch:
            if oneDaySwitch.isOn {
                trialStore.postOneDayReminder(oneDayVal: true) { (boolVal) in
                    switch (boolVal) {
                    case true:
                        print("Success in Saving One Day True")
                    case false:
                        print("Unsuccessul in saving One Day True")
                    }
                }
                
            } else {
                trialStore.postOneDayReminder(oneDayVal: false) { (boolVal) in
                    switch (boolVal) {
                    case true:
                        print("Success in Saving One Day False")
                    case false:
                        print("Unsuccessul in saving One Day False")
                    }
                }
            }
        default:
            print("nothing")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }
}
