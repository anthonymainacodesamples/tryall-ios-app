//
//  ProfileTableViewController.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 11/12/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit
import GoogleSignIn

class ProfileTableViewController: UITableViewController {
    
    //Initialize values and source classes
    var trialStore: TrialStore! = TrialStore()

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var highFrequencySwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = PRIMARY_APP_BLUE
        authKeysRefresh()
        
        
        highFrequencySwitch.addTarget(self, action: #selector(ProfileTableViewController.stateChanged(switchState:)), for: UIControlEvents.valueChanged)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //retrieveHighFreqValue()
    }
    @objc func stateChanged(switchState: UISwitch) {
        if switchState.isOn {
            //myTextField.text = "The Switch is On"
            
            trialStore.saveFrequencySelection(highFrequency: true) { (boolVal) in
                switch (boolVal) {
                case true:
                    print("Success in Saving Frequency Selection")
                case false:
                    print("Unsuccessul in saving Frequency Selection")
                }
            }
            
        } else {
            trialStore.saveFrequencySelection(highFrequency: false) { (boolVal) in
                switch (boolVal) {
                case true:
                    print("Success in Saving False Frequency Selection")
                case false:
                    print("Unsuccessul in saving False Frequency Selection")
                }
            }
        }
    }
    
    func retrieveHighFreqValue() {
        trialStore.fetchFrequencyVal { (trialsResult) in
            switch trialsResult {
            case true:
                self.highFrequencySwitch.setOn(true, animated: false)
            case false:
                self.highFrequencySwitch.setOn(false, animated: false)
            }
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        authKeysRefresh()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Table view data source

    
    func authKeysRefresh() {
        GIDSignIn.sharedInstance().signInSilently()
        let when = DispatchTime.now() + 0.4 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            if (GIDSignIn.sharedInstance().currentUser != nil) {
                let userName = GIDSignIn.sharedInstance().currentUser.profile.name
                // Use accessToken in your URL Requests Header
                self.nameLabel.text = userName
                
            } else {
                print("user object unavailable")
            }
            
            self.tableView.reloadData()
        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if (section == 1) {
            return 2
        } else {
            return 1
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Your action here
        if indexPath.section == 3 && indexPath.row == 0 {
            // do something
            // Create the AlertController and add its actions like button in ActionSheet
            let actionSheetController = UIAlertController(title: nil, message: "Are you sure?", preferredStyle: .actionSheet)
            
            let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
                //TODO: Cancel Sign Out
                
            }
            actionSheetController.addAction(cancelActionButton)
            
            let saveActionButton = UIAlertAction(title: "Yes", style: .destructive) { action -> Void in
                //TODO
                GIDSignIn.sharedInstance().signOut()
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "logIn")
                self.present(vc, animated: true, completion: nil)

            }
            actionSheetController.addAction(saveActionButton)
            
            let deleteActionButton = UIAlertAction(title: "No", style: .default) { action -> Void in
                //TODO: Don't Sign Out User
            }
            
            
            actionSheetController.addAction(deleteActionButton)
            self.present(actionSheetController, animated: true, completion: nil)
        }
        
    }

    @IBAction func highFrequencySwitchTapped(_ sender: Any) {
        
        
    }
    
}
