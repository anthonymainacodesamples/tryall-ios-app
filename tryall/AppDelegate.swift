//
//  AppDelegate.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 9/22/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit
import UserNotifications
import GoogleSignIn
import Firebase

fileprivate let viewActionIdentifier = "VIEW_IDENTIFIER"
fileprivate let reminderCategoryIdentifier = "REMINDER_CATEGORY"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"


    //TODO: Might need to capture notificaions from this method as well
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        
        application.listenForRemoteNotifications()
        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        
        // Initialize sign-in
        GIDSignIn.sharedInstance().clientID = "437530500107-sqamco1vs0ps5krnshlu9preprb6modh.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        if(GIDSignIn.sharedInstance().hasAuthInKeychain()) {
            let storyBoard: UIStoryboard = UIStoryboard(name:"Main", bundle: Bundle.main)
            let mainTabsController: UITabBarController = storyBoard.instantiateViewController(withIdentifier: "mainRoot") as! UITabBarController
            
            window?.rootViewController = mainTabsController
        }
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: { (granted, error) in
                print("Permission granted: \(granted)")
                
                guard granted else { return }
                
                let viewAction = UNNotificationAction(identifier: viewActionIdentifier,
                                                      title: "Reminder",
                                                      options: [.foreground])
                
                let reminderCategory = UNNotificationCategory(identifier: reminderCategoryIdentifier,
                                                          actions: [viewAction],
                                                          intentIdentifiers: [],
                                                          options: [])
                
                UNUserNotificationCenter.current().setNotificationCategories([reminderCategory])
            })
//            UNUserNotificationCenter.current().requestAuthorization(
//                options: authOptions,
//                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        // [END register_for_notifications]
        
        if let notification = launchOptions?[.remoteNotification] as? [String: AnyObject] {
        
            let aps = notification["aps"] as! [String: AnyObject]
        
            (window?.rootViewController as? UITabBarController)?.selectedIndex = 1
            ((window?.rootViewController as? UITabBarController)?.selectedViewController as? TrialDisplayViewController)?.tabBarItem.badgeValue = "1"
            
        }
        
        return true
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
            print("userInfo in application didReceiveRemoteNotification: \(userInfo)")
        }
        self.saveNotification(userInfo: userInfo)
        print("did receive remote notif")
        
        // Print full message.
        print(userInfo)
        
        (window?.rootViewController as? UITabBarController)?.selectedIndex = 1
        ((window?.rootViewController as? UITabBarController)?.selectedViewController as? TrialDisplayViewController)?.tabBarItem.badgeValue = "1"
    }
    
    //TODO: Pass info to viewcontroller
    func openTrialDisplayViewController(){
        print("opening trial display")
//        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let viewController : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "trialDisplay") as! TrialDisplayViewController
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//        self.window?.rootViewController = viewController
//        self.window?.makeKeyAndVisible()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
            print("User Info in didReceiveRemoteNotification: \(userInfo)")
        }
        
        self.saveNotification(userInfo: userInfo)
        print("did receive remote notif with handler")
        self.openTrialDisplayViewController()
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    private func getSavedNotifications() -> [TryallNotification]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: TryallNotification.ArchiveURL.path) as? [TryallNotification]
    }
    
    private func saveNotification(userInfo: [AnyHashable: Any]){
        
        let aps = userInfo["aps"] as! [String: Any]
        let alert = aps["alert"] as! [String: Any]
        let title = alert["title"] as! String
        let body = alert["body"] as! String
        let imageUrl = userInfo["gcm.notification.imageUrl"] as! String
        
        var toSave = [TryallNotification]()
        
        let notif = TryallNotification(title: title, body: body, imageUrl: imageUrl)
        
        if var savedNotifs = self.getSavedNotifications() {
            savedNotifs.append(notif)
            toSave = savedNotifs
        }else{
            var arr = [TryallNotification]()
            arr.append(notif)
            toSave = arr
        }
        
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(toSave, toFile: TryallNotification.ArchiveURL.path)
        if isSuccessfulSave {
            print("Successfully saved notification")
        }else{
            print("failed to save notification")
        }
    }
    // [END receive_message]
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    
    // [START openurl]
    func application(_ application: UIApplication,
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: sourceApplication,
                                                 annotation: annotation)
    }
    // [END openurl]
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }
    
    // [START signin_handler]
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
            // [START_EXCLUDE silent]
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "ToggleAuthUINotification"), object: nil, userInfo: nil)
            // [END_EXCLUDE]
        } else {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            // [START_EXCLUDE]
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "ToggleAuthUINotification"),
                object: nil,
                userInfo: [
                    "userName": "\(String(describing: (fullName)!))",
                    "idToken": idToken ?? ""
                ])
            // [END_EXCLUDE]
            
        }
    }
    // [END signin_handler]
    
    // [START disconnect_handler]
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // [START_EXCLUDE]
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: "ToggleAuthUINotification"),
            object: nil,
            userInfo: ["statusText": "User has disconnected."])
        // [END_EXCLUDE]
    }
    // [END disconnect_handler]


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
            print("User Info in userNotificationCenter willPresent : \(userInfo)")

        }
        
        self.saveNotification(userInfo: userInfo)
        print("will preent for iOS10")
        self.openTrialDisplayViewController()
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
            print("User Info in userNotificationCenter didReceive: \(userInfo)")

        }
        
        self.saveNotification(userInfo: userInfo)
        
        // Print full message.
        print(userInfo)
        
        let aps = userInfo["aps"] as! [String: AnyObject]
        print("here")
        
        (window?.rootViewController as? UITabBarController)?.selectedIndex = 1
        
        if response.actionIdentifier == viewActionIdentifier {
            let trialController = TrialDisplayViewController()
            trialController.tabBarItem.badgeValue = "1"
            window?.rootViewController?.present(trialController, animated: true, completion: nil)
        
        completionHandler()
    }
}
}
// [END ios_10_message_handling]

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        let trialStore: TrialStore! = TrialStore()
        trialStore.saveDeviceToken(deviceToken: fcmToken) { (boolVal) in
            switch (boolVal) {
            case true:
                print("Saved Firebase Notification Token")
            case false:
                print("Unsuccessful in saving firebase token")
            }
        }

    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}

