//
//  constants.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 11/13/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import Foundation
import UIKit

//MAIN APP COLORS
let PRIMARY_APP_BLUE = UIColor.init(red: 2.0/255.0, green: 20.0/255.0, blue: 66.0/255.0, alpha: 1.0)
