//
//  SearchViewControllerHeaderView.swift
//  tryall
//
//  Created by Gabriel Wamunyu on 11/23/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import Foundation
import UIKit

class SearchViewControllerHeaderView: UICollectionReusableView {
    @IBOutlet weak var lblTitle: UILabel!
}
