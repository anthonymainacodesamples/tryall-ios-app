//
//  NoResultsView.swift
//  tryall
//
//  Created by Gabriel Wamunyu on 11/25/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import Foundation
import UIKit

class NoResultsView: UIView {
    
    @IBOutlet var btnSuggestProduct: UIButton!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "NoResultsView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnSuggestProduct.layer.cornerRadius = 10
        btnSuggestProduct.layer.masksToBounds = true
        btnSuggestProduct.titleLabel!.lineBreakMode = .byWordWrapping
        btnSuggestProduct.titleLabel!.textAlignment = .center
        btnSuggestProduct.layer.borderColor = UIColor.blue.cgColor
        btnSuggestProduct.layer.borderWidth = 1
    }
}
