//
//  SearchTableViewController.swift
//  tryall
//
//  Created by Anthony Wamunyu Maina on 11/13/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit
import Alamofire
import PINRemoteImage

class SearchTableViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    
    let searchController = UISearchController(searchResultsController: nil)
    var sectionOneSearchPageItems = [SearchPageItem]()
    var sectionTwoSearchPageItems = [SearchPageItem]()
    var searchResults = [SearchPageItem]()
    var noResultsView: NoResultsView?
    var trialStore = TrialStore()
    var searchText: String?

    override func viewDidLoad() {
        super.viewDidLoad()
            // Setup the Search Controller
        //searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search by name, type, keywords"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        collectionView?.registerReusableCell(SingleTrialCell.self)
        collectionView?.contentInset = UIEdgeInsets(top: 23, left: 16, bottom: 10, right: 16)
        
        collectionView?.register(UINib(nibName: "InterestCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "InterestCell")
        collectionView?.register(UINib(nibName: "SeeMore", bundle: nil), forCellWithReuseIdentifier: "seemore")
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        
        fetchCategories()
        fetchRecentlyAdded()
    
    }
    
    func fetchCategories(){
        
        trialStore.fetch10RandomBroadCategories(completion: {(interestsResults) in
            switch (interestsResults) {
            case let .success(categories):
                if categories.count > 0 {
                    for item in categories {
                        let category = SearchPageItem(interest: item, type: .category)
                        self.sectionOneSearchPageItems.append(category)
                    }
                    
                    let seemore = SearchPageItem(type: .seemore)
                    self.sectionOneSearchPageItems.append(seemore)
                    
                    self.collectionView?.reloadData()
                }else{
                    print("no categories")
                }
            case let .failure(error):
                print("Broad categories not found: \(error)")
                
            }
        })
        
    
    }
    
    func fetchRecentlyAdded(){
        
        trialStore.fetch10RecentlyAddedTrials(completion: {(trialsResults) in
            switch (trialsResults) {
            case let .success(trials):
                if trials.count > 0 {
                    for item in trials {
                        let trial = SearchPageItem(trial: item, type: .trial)
                        self.sectionTwoSearchPageItems.append(trial)
                    }
                    self.collectionView?.reloadData()
                }else{
                    print("no trials")
                }
            case let .failure(error):
                print("Recently added trials not found: \(error)")
                
            }
        })
    }
    
    @objc func suggestProduct(){
        let alert = UIAlertController(title: "Free Trial", message: "Kindly provide the name of the product we should look into", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addTextField(configurationHandler: {(textField: UITextField) in
            textField.text = self.searchText
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    func fetchSearchResults(searchText: String){
        
        trialStore.fetchSearchResults(searchText: searchText, completion: {(searchResults) in
            
            switch (searchResults) {
            case let .success(searches):
                if searches.count > 0 {
                    for item in searches {
                        let searchItem = SearchPageItem(search: item, type: .searchResult)
                        self.searchResults.append(searchItem)
                    }
                    self.collectionView?.reloadData()
                }else{
                    self.noResultsView = NoResultsView.instanceFromNib() as! NoResultsView
                    self.noResultsView?.btnSuggestProduct.addTarget(self, action: #selector(self.suggestProduct), for: .touchUpInside)
                    self.collectionView?.reloadData()
                    self.collectionView?.addSubview(self.noResultsView!)
                }
            case let .failure(error):
                print("Error in fetching search results: \(error)")

            }
        })
    }

   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if #available(iOS 11.0, *) {
            // TODO : introduces bug where searchbar is pushed to top on back navigation from TrialDisplayViewController
            //navigationItem.hidesSearchBarWhenScrolling = true
        }
    }
    
    @objc func openMoreInterests(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "interests") as! InterestsViewController
        self.navigationController?.pushViewController(newViewController, animated: true)
    }

    //MARK: - CollectionView Delegate Methods
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.searchResults.count != 0 {
            return self.searchResults.count
        }
        
        if section == 0 {
            return self.sectionOneSearchPageItems.count
        }
        
        return self.sectionTwoSearchPageItems.count
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        if self.searchResults.count != 0 {
            return 1
        }
        
        if self.sectionOneSearchPageItems.count == 0 || self.sectionTwoSearchPageItems.count == 0 {
            return 0
        }
        return 2
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: "SearchHeaderView",
                                                                             for: indexPath) as! SearchViewControllerHeaderView
            if indexPath.section == 0 {
                
                if self.searchResults.count != 0 {
                   headerView.lblTitle.text = "Search Results"
                }else{
                  headerView.lblTitle.text = "Categories of Free Trials"
                }
                return headerView
            }else{
                headerView.lblTitle.text = "Recently added trials"
                return headerView
            }
            
        default:
            assert(false, "Unexpected element kind")
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.searchResults.count > 0 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "trialDisplay") as! TrialDisplayViewController
            newViewController.selectedTrialID = self.searchResults[indexPath.row].searchResult?.id
            self.navigationController?.pushViewController(newViewController, animated: true)
        }else{
            if indexPath.section == 1 {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "trialDisplay") as! TrialDisplayViewController
                newViewController.selectedTrialID = self.sectionTwoSearchPageItems[indexPath.row].trial?.tryallID
                self.navigationController?.pushViewController(newViewController, animated: true)
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0 {
            
            if self.searchResults.count != 0 {
                
                let item = self.searchResults[indexPath.row]
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchResultViewCell", for: indexPath as IndexPath) as! SearchResultViewCell
                
                cell.lblName.text = item.searchResult!.name
                cell.lblTagLine.text = item.searchResult!.tagLine
                cell.logoImage.pin_setImage(from: URL(string: item.searchResult!.logoLink))
                cell.id = item.searchResult!.id
                return cell
            }
            
            let item = self.sectionOneSearchPageItems[indexPath.row]
            
            switch item.type {
            case .category:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InterestCell", for: indexPath as IndexPath) as! InterestCollectionViewCell
                cell.btnCategory.setTitle(item.interest!.name, for: .normal)
                return cell
            case .searchResult:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchResultViewCell", for: indexPath as IndexPath) as! SearchResultViewCell
                
                cell.lblName.text = item.searchResult!.name
                cell.lblTagLine.text = item.searchResult!.tagLine
                cell.logoImage.pin_setImage(from: URL(string: item.searchResult!.logoLink))
                return cell
            case .seemore:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "seemore", for: indexPath) as! SeeMore
                cell.btnSeeMore.addTarget(self, action: #selector(self.openMoreInterests), for: .touchUpInside)
                return cell
            case .trial: // Should not come here
                let cell = SingleTrialCell.dequeue(fromCollectionView: collectionView, atIndexPath:indexPath)
                cell.tiralDescription.text = item.trial?.description
                cell.trialDuration.text = String(item.trial?.description ?? "")
                cell.trialMonthlyCost.text = String(item.trial?.MonthlyDollarPrice ?? 0)
                cell.logoImageView.pin_setImage(from: item.trial?.logoLink)
                return cell
            }
        }else{
            let cell = SingleTrialCell.dequeue(fromCollectionView: collectionView, atIndexPath:indexPath)
            let item = self.sectionTwoSearchPageItems[indexPath.row]
            cell.tiralDescription.text = item.trial?.description
            cell.trialDuration.text = String(item.trial?.description ?? "")
            cell.trialMonthlyCost.text = String(item.trial?.MonthlyDollarPrice ?? 0)
            cell.logoImageView.pin_setImage(from: item.trial?.logoLink)
            return cell
        }
    
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.section == 0  {
            
            if self.searchResults.count != 0 {
                let itemSize = (collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right + 10)) / 2
                return CGSize(width: itemSize, height: itemSize)
            }
            
            let item = self.sectionOneSearchPageItems[indexPath.row]
            
            switch item.type {
            case .category:
                let itemSize = (collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right + 10)) / 2
                return CGSize(width: itemSize, height: 80)
            case .searchResult:
                let itemSize = (collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right + 10)) / 2
                return CGSize(width: itemSize, height: itemSize)
            case .trial: // Should not come here
                if UIDevice.current.userInterfaceIdiom == .phone {
                    return CGSize(width: collectionView.bounds.width, height: BaseRoundedCardCell.cellHeight)
                } else {
                    
                    //iPhone Attributes
                    
                    
                    // Number of Items per Row
                    let numberOfItemsInRow = 2
                    
                    // Current Row Number
                    let rowNumber = indexPath.item/numberOfItemsInRow
                    
                    // Compressed With
                    let compressedWidth = collectionView.bounds.width/3
                    
                    // Expanded Width
                    let expandedWidth = (collectionView.bounds.width/3) * 2
                    
                    // Is Even Row
                    let isEvenRow = rowNumber % 2 == 0
                    
                    // Is First Item in Row
                    let isFirstItem = indexPath.item % numberOfItemsInRow != 0
                    
                    // Calculate Width
                    var width: CGFloat = 0.0
                    if isEvenRow {
                        width = isFirstItem ? compressedWidth : expandedWidth
                    } else {
                        width = isFirstItem ? expandedWidth : compressedWidth
                    }
                    
                    return CGSize(width: width, height: BaseRoundedCardCell.cellHeight)
                }
            case .seemore:
                let itemSize = (collectionView.frame.width)
                return CGSize(width: itemSize, height: 50)
            }
        }else{
            if UIDevice.current.userInterfaceIdiom == .phone {
                return CGSize(width: collectionView.bounds.width, height: BaseRoundedCardCell.cellHeight)
            } else {
                
                //iPhone Attributes
                
                
                // Number of Items per Row
                let numberOfItemsInRow = 2
                
                // Current Row Number
                let rowNumber = indexPath.item/numberOfItemsInRow
                
                // Compressed With
                let compressedWidth = collectionView.bounds.width/3
                
                // Expanded Width
                let expandedWidth = (collectionView.bounds.width/3) * 2
                
                // Is Even Row
                let isEvenRow = rowNumber % 2 == 0
                
                // Is First Item in Row
                let isFirstItem = indexPath.item % numberOfItemsInRow != 0
                
                // Calculate Width
                var width: CGFloat = 0.0
                if isEvenRow {
                    width = isFirstItem ? compressedWidth : expandedWidth
                } else {
                    width = isFirstItem ? expandedWidth : compressedWidth
                }
                
                return CGSize(width: width, height: BaseRoundedCardCell.cellHeight)
            }
        }
        
    }
}


extension SearchTableViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        self.searchText = searchBar.text!
        self.sectionOneSearchPageItems.removeAll()
        self.sectionTwoSearchPageItems.removeAll()
        
        if self.noResultsView?.window != nil {
            self.noResultsView?.removeFromSuperview()
        }
        fetchSearchResults(searchText: self.searchText!)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchResults.removeAll()
        
        if self.sectionOneSearchPageItems.count > 0 {
            self.sectionOneSearchPageItems.removeAll()
        }
        
        if self.sectionTwoSearchPageItems.count > 0 {
            self.sectionTwoSearchPageItems.removeAll()
        }
        
        if self.noResultsView?.window != nil {
            self.noResultsView?.removeFromSuperview()
        }
        
        self.collectionView?.reloadData()
        
        self.fetchCategories()
        self.fetchRecentlyAdded()
        
    }
}
