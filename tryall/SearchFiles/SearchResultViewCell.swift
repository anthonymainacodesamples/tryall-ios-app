//
//  SearchResultViewCell.swift
//  tryall
//
//  Created by Gabriel Wamunyu on 11/15/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import UIKit

class SearchResultViewCell: UICollectionViewCell {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblTagLine: UILabel!
    @IBOutlet var containerView: UIView!
    @IBOutlet var logoImage: UIImageView!
    var id: String? = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 6
        containerView.layer.masksToBounds = true
        
    }
    
    
}
