//
//  SearchResult.swift
//  tryall
//
//  Created by Gabriel Wamunyu on 11/23/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import Foundation

class SearchResult {
    
    var name: String
    var logoLink: String
    var tagLine: String
    var id: String
    
    init(data: [String: Any]) {
        
        self.id = data["_id"] as! String
        self.name = data["name"] as! String
        self.logoLink = data["logoLink"] as! String
        
        if let logoLink = data["logoLink"] as? String{
            self.logoLink = logoLink
        }else{
            self.logoLink = "https://bizcentralusa.com/wp-content/uploads/2016/05/placeholder-sample-logo.png"
        }
        
        if let tagLine = data["tagLine"] as? String{
            self.tagLine = tagLine
        }else{
            self.tagLine = "Sample tagline"
        }
    }
}
