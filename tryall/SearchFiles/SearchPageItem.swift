//
//  SearchItem.swift
//  tryall
//
//  Created by Gabriel Wamunyu on 11/15/17.
//  Copyright © 2017 Anthony Wamunyu Maina. All rights reserved.
//

import Foundation

class SearchPageItem {
    
    enum SearchPageItemType {
        case category
        case trial
        case searchResult
        case seemore
    }
    
    var interest: Interest?
    var searchResult: SearchResult?
    var trial: Trial?
    
    var seeMoreText: String?
    
    var type: SearchPageItemType
    
    init(data : [String: Any], type: SearchPageItemType) {
        
        switch type {
        case .category:
            let interest = Interest(data: data)
            self.interest = interest
        case .searchResult:
            let searchResult = SearchResult(data: data)
            self.searchResult = searchResult
        case .trial:
            self.trial = TryallAPI.trial(fromJSON: data)
        default:
            assert(false, "Should not come here")
        }
        
        self.type = type
    }
    
    init(interest: Interest, type: SearchPageItemType) {
        self.interest = interest
        self.type = type
    }
    
    init(trial: Trial, type: SearchPageItemType) {
        self.trial = trial
        self.type = type
    }
    
    init(search: SearchResult, type: SearchPageItemType) {
        self.searchResult = search
        self.type = type
    }
    
    init(type: SearchPageItemType) {
        if type != .seemore {
            print("type should always be see more")
        }
        self.type = type
    }
    
    
}
